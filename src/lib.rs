use std::net::TcpStream;
use std::io::{Read, Write};
use std::io::BufReader;
use std::io::BufRead;
use std::io::BufWriter;
pub struct TidyClient {
    address: String,
}

impl TidyClient {
    pub fn new(address: impl Into<String>) -> Self {
        Self {
            address: address.into(),
        }
    }
    pub fn tidyup(&self, html_data: String) -> String {
        //ureq::post(self.base_url.as_str()).send_string(html_data.as_str()).unwrap().into_string().unwrap()
        let mut stream = TcpStream::connect(self.address.clone()).unwrap();
        handle_write(&mut stream, html_data);
        handle_read(&mut stream)
    }
}

fn handle_read(con: &mut impl Read) -> String {
    let mut b = BufReader::new(con);
    let mut length_string = String::new();
    b.read_line(&mut length_string).unwrap();
    let length = length_string.trim().parse::<u64>().unwrap();
    let mut c = b.take(length);
    let mut html_data = String::new();
    c.read_to_string(&mut html_data).unwrap();
    html_data
}

fn handle_write(con: &mut impl Write, html_data: String) {
    let mut b = BufWriter::new(con);
    let length = html_data.len();
    let data = format!("{}\n{}", length, html_data);
    b.write_all(&data.as_bytes()).unwrap();
}
